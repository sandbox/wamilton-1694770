<?php
/**
 * @file
 * drush hooks for provision_deploy
 */

/**
 * Implements drush_hook_COMMAND().
 */
function drush_safe_deploy_provision_deploy() {
  // Prove that this gets invoked.
  echo "\n\nWOWZERS\n\n";
  die();
}
