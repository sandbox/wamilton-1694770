<?php

function drush_safe_deploy_pre_updatedb() {
  drush_invoke_process(d()->name, 'registry-rebuild');
}
