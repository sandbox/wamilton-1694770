<?php

include_once(dirname(__FILE__) . '/../provision.service.inc');

function safe_deploy_provision_services() {
  return array('safe_deploy' => NULL);
}


// Base safe_deploy service class. 
class provisionService_safe_deploy extends provisionService {
  public $service = 'safe_deploy';
}
